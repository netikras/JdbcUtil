

Created this tiny util when I needed to test some ORA drivers and see how they behave in my infra. Added a few bells and whistles since then. Still it is a very simple though sometimes very useful util.

### USAGE

```
At least 3 arguments must be provided.
USAGE:
  * -u   <username>
  * -p   <password>
  * -U   <url>       : e.g.: localhost:1521:mydatabase; jdbc:oracle:thin:@localhost:1521:mydatabase
    -q   <query>
    -d   <delimiter> : delimiter to separate columns in SELECT output
    -dr  <driver>    : custom driver class name, e.g.: -dr 'org.h2.Driver'
    -pg  <page>      : pages to select. e.g. 3-4:20 will print pages 3 and 4 of size 20. 3:20 will only print pg #3
    -if  <file>      : query input file. Either 'file:///some/path/to/file.sql' or '--' for stdin
    -o   <option>    : provide additional options. See docs for more info: https://gitlab.com/netikras/JdbcUtil
```


### OPTIONS

Options are there to alter tool's behavior. They can be passed with `-o` parameter as flags. Currently available options:

 - nonull - select queries print text '(null)' where value is _null_ by default. If this flag is set nothing is printed instead

 - blob - select queries print text '(blob)' where return value is lob. If this flag is set base64 encoded blobs are printed instead

 - noRowNum - if flag is set rownum values will not be printed at the beginning of each result line

 - noHeader - if flag is set output will not contain column names at the top of result set

 - unsafe - by default only safe queries are allowed (select, update, delete, insert, ...). Setting this flag allows any query to be run (as executeUpdate())


### EXAMPLE

    netikras@netikras-xps /tmp/playground/JdbcUtil $ java -cp .:/home/netikras/received/h2/h2-1.4.192.jar JdbcUtil -u root -p test123 -U jdbc:h2:mem:test1 -q "show databases"  | column -s '|' -t
    Trying driver: oracle.jdbc.driver.OracleDriver
    Trying driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
    Trying driver: com.mysql.jdbc.Driver
    Trying driver: com.ibm.db2.jcc.DB2Driver
    Trying driver: org.h2.Driver
    Loaded JDBC driver: [org.h2.Driver]
    Connecting to database: jdbc:h2:mem:test1 using credentials: root:******...
    Connected
    Row#  SCHEMA_NAME
    1     INFORMATION_SCHEMA
    2     PUBLIC
    netikras@netikras-xps /tmp/playground/JdbcUtil $ 


    netikras@netikras-xps /tmp/playground/JdbcUtil $ java -cp .:/home/netikras/received/h2/h2-1.4.192.jar JdbcUtil -u root -p test123 -U jdbc:h2:mem:test1 -q "show databases"  -dr org.h2.Driver 2>/dev/null | column -s '|' -t
    Row#  SCHEMA_NAME
    1     INFORMATION_SCHEMA
    2     PUBLIC
    netikras@netikras-xps /tmp/playground/JdbcUtil $ 

    netikras@netikras-xps /tmp/playground/JdbcUtil $ java -cp .:/home/netikras/received/h2/h2-1.4.192.jar JdbcUtil -u root -p test123 -U jdbc:h2:mem:test1 -q "show databases"  -dr org.h2.Driver 2>/dev/null -o noHeader -o noRowNum | column -s '|' -t
    INFORMATION_SCHEMA
    PUBLIC
    netikras@netikras-xps /tmp/playground/JdbcUtil $ 



#### NOTE:

It might be a good idea to add `-Djava.security.egd=file:///dev/urandom` on Linux/UNIX platforms. Otherwise client might time-out while authenticating. 

For testing feel free to use the H2 test database file testDB.mv.db (download the `h2-1.4.200.jar` separately):

```
java -cp ~/received/h2/bin/h2-1.4.200.jar:. JdbcUtil -u user -p pass -U 'jdbc:h2:file:./testDB' -dr org.h2.Driver -q "select * from test_table;"
```


