#!/bin/bash

[ -n "${JAVA_HOME}" ] || { JAVA_HOME=$(readlink -f $(which java)) && JAVA_HOME=${JAVA_HOME%/bin/java}; }
JAVA_BIN=${JAVA_HOME:+${JAVA_HOME}/bin}

[ -n "${JAVA_BIN}" ] || { echo "Could not locate JAVA_BIN. JAVA_HOME=${JAVA_HOME}." >&2; exit 1; }

"${JAVA_BIN}/javac" JdbcUtil.java && \
    "${JAVA_BIN}/jar" -cvfm JdbcUtil.jar manifest.mf *.class 
#    rm -f *.class

