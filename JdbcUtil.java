import java.sql.*;
import java.util.*;
import java.io.*;
import java.lang.*;

public class JdbcUtil {

    private static Connection conn = null;
    private static Configuration config;

    public static void main(String[] args) {
        config = new Configuration();

        parseParams(args, config);

        try {
            String driverName = config.findDriver();
            if (driverName == null) {
                System.err.println("Cannot find any known JDBC driver in the classpath.");
                System.exit(1);
            }
            Class.forName(driverName);

            readQuery(config);

            System.err.println(
                    "Connecting to database: " + config.getUrl() + " using credentials: " + config.getUsername() + ":" + "******" + "...");
            //conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn = getConnection(config);
            System.err.println("Connected");
            for (String singleQuery : getQueries(config)) { // FIXME not safe!
                sendQuery(conn, config, singleQuery.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeQuietly(conn);
        }
    }

    private static Connection getConnection(Configuration config) throws SQLException {
        return DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
    }

    private static String[] getQueries(Configuration config) {
        if (config.getQry() != null) {
            return config.getQry().split(";");
        } else {
            return new String[] {};
        }
    }

    private static void parseParams(String[] params, Configuration config) {
        config.parseParams(params);
    }

    private static Configuration getConfig() {
        return config;
    }

    private static String readQuery(Configuration config) {

        InputStream is = null;

        if (config.getQryFile() == null || config.getQryFile().isEmpty()) {
            return config.getQry();
        }

        if ("--".equals(config.getQryFile())) {
            System.err.println("Reading query from stdin");
            try {
                config.setQry(inputStreamToString(System.in, config.getEncoding()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return config.getQry();
        }

        if (config.getQryFile().startsWith("file://")) {
            System.err.println("Reading query from file: " + config.getQryFile());
            config.setQryFile(config.getQryFile().split("file://")[1]);
            try {
                is = new FileInputStream(config.getQryFile());
                config.setQry(inputStreamToString(is, config.getEncoding()));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeQuietly(is);
            }
            return config.getQry();
        }

        return null;
    }

    private static String inputStreamToString(InputStream inputStream, String encoding) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        // StandardCharsets.UTF_8.name() > JDK 7
        try {
            return result.toString(encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void sendQuery(Connection connection, Configuration config, String qry) {
        String qry_low = qry.toLowerCase();
        //    qry = qry.toLowerCase();
        if (qry_low.startsWith("select")) {
            System.err.println("Sending query: [" + qry + "]");
            sendSelectQuery(connection, config, qry);
        } else if (qry_low.startsWith("insert")) {
            sendInsertQuery(connection, qry);
        } else if (qry_low.startsWith("show")) {
            sendSelectQuery(connection, config, qry);
        } else if (qry_low.startsWith("update")) {
            if (!qry_low.contains(" where ")) {
                System.err.println("WHERE clause is missing.");
                System.exit(4);
            }

            sendUpdateQuery(connection, qry);
        } else if (qry_low.startsWith("delete")) {
            if (!qry_low.contains(" where ")) {
                System.err.println("WHERE clause is missing.");
                System.exit(4);
            }

            sendDeleteQuery(connection, qry);
        } else if (config.isAllowUnsafe()) {
            System.err.println("Executing unsafe query: \n" + qry);
            sendUnsafeQuery(connection, qry);
        } else {
            System.err.println("Unrecognized query: [" + qry + "]");
        }
    }

    private static void sendSelectQuery(Connection connection, Configuration config, String query) {
        ResultSet rs = null;
        try {
            Statement stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            ResultSetMetaData meta = rs.getMetaData();
            int colsCnt = meta.getColumnCount();
            int rownum = 0;
            String data;

            long currentPage = 0;
            long currentPageRow = -1;

            if (config.isNoHeader() == false) {
                System.out.print("Row#");
                for (int i = 1; i <= colsCnt; i++) {
                    System.out.print(config.getDelim() + meta.getColumnName(i));
                }
                System.out.println();
            }

            String nulltext = "(null)";
            String delim = "";
            if (config.isHideNull()) {
                nulltext = "";
            }

            while (rs.next()) {
                rownum++;
                currentPageRow++;
                if (currentPageRow == config.getPageSize()) {
                    currentPage++;
                    currentPageRow = 0;
                    if (currentPage >= config.getPageStart() && currentPage <= config.getPageEnd()) {
                        System.err.println("Page #" + currentPage);
                    }
                }
                if (currentPage >= config.getPageStart()) {
                    if (currentPage > config.getPageEnd()) {
                        System.err.println("End of last page: #" + config.getPageEnd());
                        break;
                    }
                } else {
                    continue;
                }

                delim = "";
                if (config.isNoRowNum() == false) {
                    System.out.print(rownum);
                    delim = config.getDelim();
                }

                for (int i = 1; i <= colsCnt; i++) {
                    Object value = rs.getObject(i);
                    if (value == null) {
                        System.out.print(delim + nulltext);
                    } else if (Blob.class.isAssignableFrom(value.getClass())) {
                        if (config.isPrintBlob()) {
                            try {
                                Blob blob = (Blob) value;
                                System.out.println(delim + encodeBase64(blob.getBytes(1, (int) blob.length())));
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.print(delim + "(!blob)");
                            }
                        } else {
                            System.out.print(delim + "(blob)");
                        }
                    } else if (Clob.class.isAssignableFrom(value.getClass())) {
                        if (config.isPrintClob()) {
                            try {
                                Clob clob = (Clob) value;
                                System.out.print(delim + toString(clob.getCharacterStream(1, (int) clob.length())));
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.print(delim + "(!clob)");
                            }
                        } else {
                            System.out.print(delim + "(clob)");
                        }
                    } else {
                        System.out.print(delim + rs.getObject(i));
                    }

                    delim = config.getDelim();
                }

                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(3);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
        }

    }

    private static String toString(Reader reader) throws IOException {
        char[] arr = new char[8 * 1024];
        StringBuilder buffer = new StringBuilder();
        int numCharsRead;
        while ((numCharsRead = reader.read(arr, 0, arr.length)) != -1) {
            buffer.append(arr, 0, numCharsRead);
        }
        reader.close();
        return buffer.toString();
    }

    private static void sendUnsafeQuery(Connection connection, String query) {
        try {
            Statement stmt = connection.createStatement();
            int updatedCount = stmt.executeUpdate(query);

            System.out.println("Updated rows: " + updatedCount);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(3);
        }

    }

    private static void sendUpdateQuery(Connection connection, String query) {
        try {
            Statement stmt = connection.createStatement();
            int updatedCount = stmt.executeUpdate(query);

            System.out.println("Updated rows: " + updatedCount);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(3);
        }

    }

    private static void sendInsertQuery(Connection connection, String query) {
        try {
            Statement stmt = connection.createStatement();
            int updatedCount = stmt.executeUpdate(query);

            System.out.println("Inserted rows: " + updatedCount);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(3);
        }

    }

    private static void sendDeleteQuery(Connection connection, String query) {
        try {
            Statement stmt = connection.createStatement();
            int updatedCount = stmt.executeUpdate(query);

            System.out.println("Deleted rows: " + updatedCount);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(3);
        }

    }

    private static void closeQuietly(Connection closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void closeQuietly(Closeable closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String encodeBase64(byte[] data) {
        sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
        return encoder.encode(data);
    }

    static final String USAGE = ""
            + "USAGE:\n"
            + "  * -u   <username>\n"
            + "  * -p   <password>\n"
            + "  * -U   <url>       : e.g.: localhost:1521:mydatabase; jdbc:oracle:thin:@localhost:1521:mydatabase \n"
            + "    -q   <query>\n"
            + "    -d   <delimiter> : delimiter to separate columns in SELECT output\n"
            + "    -dr  <driver>    : custom driver class name, e.g.: -dr 'org.h2.Driver'\n"
            + "    -pg  <page>      : pages to select. e.g. 3-4:20 will print pages 3 and 4 of size 20. 3:20 will only print pg #3\n"
            + "    -if  <file>      : query input file. Either 'file:///some/path/to/file.sql' or '--' for stdin\n"
            + "    -o   <option>    : provide additional options. See docs for more info: https://gitlab.com/netikras/JdbcUtil"
            + "\n";

    private static class Configuration {

        private final List<String> DRIVERS;

        public Configuration() {
            DRIVERS = new ArrayList<String>();
            DRIVERS.add("oracle.jdbc.driver.OracleDriver");
            DRIVERS.add("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            DRIVERS.add("com.microsoft.sqlserver.jdbc.SQLServerConnectionPoolDataSource");
            DRIVERS.add("com.mysql.jdbc.Driver");
            DRIVERS.add("org.mariadb.jdbc.Driver");
            DRIVERS.add("com.ibm.db2.jcc.DB2Driver");
            DRIVERS.add("org.h2.Driver");
        }

        //private final static String DB_URL = "jdbc:oracle:thin:@localhost:1521:mydatabase";
        //private final static String USER = "UsernAme";
        //private final static String PASS = "password123";

        private String url_base = "jdbc:oracle:thin:";

        private String username = "";
        private String password = "";
        private String url = "";

        private String qry = null;
        private String qryFile = null;
        private String delim = "|";

        private String encoding = "UTF-8";

        private boolean printBlob = false;
        private boolean printClob = false;
        private boolean hideNull = false;
        private boolean noRowNum = false;
        private boolean noHeader = false;
        private boolean allowUnsafe = false;

        private long pageSize = -1;
        private long pageStart = 0;
        private long pageEnd = 0;

        public void parseParams(String[] params) {
            if (params.length < 3) {
                System.err.println("At least 3 arguments must be provided.");
                System.out.println(USAGE);
                System.exit(1);
            }

            for (int i = 0; i < params.length; i++) {
                String arg = params[i];

                /* java6 is unable to switch strings */
                // @formatter:off
            if ("-u".equals(arg))   { setUsername(params[++i]);  } else
            if ("-p".equals(arg))   { setPassword(params[++i]);  } else
            if ("-U".equals(arg))   { setUrl(params[++i]);  } else
            if ("-q".equals(arg))   { setQry(params[++i]);  } else
            if ("-d".equals(arg))   { setDelim(params[++i]);  } else
            if ("-dr".equals(arg))  { addDriver(params[++i]);  } else
            if ("-if".equals(arg))  { setQryFile(params[++i]);  } else
            if ("-pg".equals(arg))  { parsePages(params[++i]); } else
            if ("-o".equals(arg))   { parseOpt(getConfig(), params[++i]);   } else
            {
                System.err.println("Unknown argument: " + arg);
                System.err.println(USAGE);
                System.exit(1);
            }
            // @formatter:on
            }

        }

        public void parseOpt(Configuration config, String option) {
            if (option == null || option.isEmpty())
                return;

            if ("blob".equals(option)) {
                config.setPrintBlob(true);
            } else if ("nonull".equals(option)) {
                config.setHideNull(true);
            } else if ("noRowNum".equals(option)) {
                config.setNoRowNum(true);
            } else if ("noHeader".equals(option)) {
                config.setNoHeader(true);
            } else if ("clob".equals(option)) {
                config.setPrintClob(true);
            } else if ("unsafe".equals(option)) {
                config.setAllowUnsafe(true);
            } else 
                System.err.println("Unknown option: " + option);
        }

        public void parsePages(String expr) {
            if (expr == null || expr.isEmpty()) {
                return;
            }

            String pagesizeStr = "0";
            String pageStartStr = "1";
            String pageEndStr = "1";

            long _pagesize = 0;
            long _pageStart = 0;
            long _pageEnd = 0;

            String[] tokens = expr.split(":");
            if (tokens != null && tokens.length > 0) {
                if (tokens.length > 1) {
                    pagesizeStr = tokens[1];
                    if (pagesizeStr != null && !pagesizeStr.isEmpty()) {
                        _pagesize = Long.parseLong(pagesizeStr);
                    }
                }

                tokens = tokens[0].split("-");
                if (tokens != null && tokens.length > 0) {
                    pageStartStr = tokens[0];
                    pageEndStr = pageStartStr;
                    if (tokens.length > 1) {
                        pageEndStr = tokens[1];
                    }

                    if (pageStartStr != null && !pageStartStr.isEmpty()) {
                        _pageStart = Long.parseLong(pageStartStr);
                    } else {
                        _pageStart = 0;
                    }

                    if (pageEndStr != null && !pageEndStr.isEmpty()) {
                        _pageEnd = Long.parseLong(pageEndStr);
                    } else {
                        _pageEnd = pageStart;
                    }

                }
            }

            if (_pageStart > _pageEnd) {
                throw new IllegalStateException("Starting page number cannot be lower than ending page");
            }

            System.err.println(String.format("Start page: %s, end page: %s, page size: %s", _pageStart, _pageEnd, _pagesize));

            pageSize = _pagesize;
            pageStart = _pageStart;
            pageEnd = _pageEnd;
        }


        public String findDriver() {

            for (String driver : DRIVERS) {
                try {
                    System.err.println("Trying driver: " + driver);
                    Class.forName(driver, false, JdbcUtil.class.getClassLoader());
                    System.err.println("Loaded JDBC driver: [" + driver + "]");
                    return driver;
                } catch (Exception e) {
                }
            }
            return null;
        }

        public void addDriver(String driverClassName) {
            System.err.println("Adding driver to the list: " + driverClassName);
            DRIVERS.set(0, driverClassName);
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            if (!url.toLowerCase().startsWith("jdbc")) {
                url = url_base + "@" + url;
            }

            this.url = url;
        }

        public String getQry() {
            return qry;
        }

        public void setQry(String qry) {
            this.qry = qry;
        }

        public String getQryFile() {
            return qryFile;
        }

        public void setQryFile(String qryFile) {
            this.qryFile = qryFile;
        }

        public String getDelim() {
            return delim;
        }

        public void setDelim(String delim) {
            this.delim = delim;
        }

        public String getEncoding() {
            return encoding;
        }

        public void setEncoding(String encoding) {
            this.encoding = encoding;
        }

        public boolean isPrintBlob() {
            return printBlob;
        }

        public void setPrintBlob(boolean printBlob) {
            this.printBlob = printBlob;
        }

        public boolean isPrintClob() {
            return printClob;
        }

        public void setPrintClob(boolean printClob) {
            this.printClob = printClob;
        }

        public boolean isHideNull() {
            return hideNull;
        }

        public void setHideNull(boolean hideNull) {
            this.hideNull = hideNull;
        }

        public boolean isNoRowNum() {
            return noRowNum;
        }

        public void setNoRowNum(boolean noRowNum) {
            this.noRowNum = noRowNum;
        }

        public boolean isNoHeader() {
            return noHeader;
        }

        public void setNoHeader(boolean noHeader) {
            this.noHeader = noHeader;
        }

        public long getPageSize() {
            return pageSize;
        }

        public void setPageSize(long pageSize) {
            this.pageSize = pageSize;
        }

        public long getPageStart() {
            return pageStart;
        }

        public void setPageStart(long pageStart) {
            this.pageStart = pageStart;
        }

        public long getPageEnd() {
            return pageEnd;
        }

        public void setPageEnd(long pageEnd) {
            this.pageEnd = pageEnd;
        }

        public void setAllowUnsafe(boolean allow) {
            this.allowUnsafe = allow;
        }

        public boolean isAllowUnsafe() {
            return this.allowUnsafe;
        }
    }

}
